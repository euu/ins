#!/bin/sh
# Create folder in RAM and use it for VirtualBox VMs files
set -eu
if [ "$(vboxmanage list systemproperties | grep machine | awk '{print $5}')" = "VMs" ];then
sudo mkdir -p /mnt/tmpfs
sudo mount -t tmpfs -o size=30G tmpfs /mnt/tmpfs && df -h /mnt/tmpfs
vboxmanage setproperty machinefolder /mnt/tmpfs
sudo sed -n '/\/mnt\/tmpfs/p' /etc/mtab | sudo tee -a /etc/fstab >/dev/null
echo -n VirtualBoxs
vboxmanage list systemproperties | grep machine
else
vboxmanage setproperty machinefolder "$HOME/VirtualBox VMs"
sudo umount /mnt/tmpfs
sudo sed -i '/\/mnt\/tmpfs/d' /etc/fstab && echo "/mnt/tmpfs mount deleted"
echo -n VirtualBoxs
vboxmanage list systemproperties | grep machine
fi