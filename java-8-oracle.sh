echo Installing Oracle JDK 8
[ -d /opt/java ] && rm -rf /opt/java/* || mkdir -p /opt/java
url=$(curl -s https://www.java.com/en/download/linux_manual.jsp | grep -oP '(?<=x64\"\ href=\")https:\/\/javadl\.oracle\.com\/webapps\/download\/AutoDL\?BundleId\=([0-9_a-f]+)')
curl -L $url | tar xz -C /opt/java --strip-components 1
update-alternatives --install /usr/bin/java java /opt/java/bin/java 2000 
echo /opt/java/lib/amd64/jli | tee -a /etc/ld.so.conf && ldconfig
mkdir -p /usr/lib/mozilla/plugins/
ln -sf /opt/java/lib/amd64/libnpjp2.so /usr/lib/mozilla/plugins/libnpjp2.so
java -version