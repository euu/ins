#!/bin/sh
# Install latest Golang
set -eu

url=$(wget -qO- https://golang.org/dl/ | grep -oP '\/dl\/go([0-9\.]+)\.linux-amd64\.tar\.gz' | head -1 )
version=$(echo $url | grep -Po '(?<=go)\d+.\d+.?\d+')
if type go 1>/dev/null;then
  installed=$(go version | grep -Po '\d+.\d+.?\d+')
  if [ "$version" = "$installed" ]; then
  echo There\'s no need to update
  exit 0
  fi
fi
echo Installing Golang $version
if [ -d /opt/go ];then
  sudo rm -rf /opt/go
else
  sudo ln -s /opt/go/bin/go /usr/local/bin/go
  sudo ln -s /opt/go/bin/gofmt /usr/local/bin/gofmt
fi

sudo wget -O- https://golang.org$url | sudo tar xz -C /opt
mkdir -p ~/go/bin
mkdir -p ~/go/pkg
mkdir -p ~/go/src

if ! grep -q GOPATH ~/.bashrc ;then
  { echo '# GoLang'
    echo 'export GOPATH=~/go'
    echo 'export PATH=$PATH:$GOPATH/bin'
  } >> ~/.bashrc
  echo GOPATH added to PATH
fi
go version
