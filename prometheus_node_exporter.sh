adduser --no-create-home --disabled-login --shell /bin/false --gecos "Prometheus Monitoring User" prometheus
version=$(wget -qO- https://raw.githubusercontent.com/prometheus/node_exporter/master/VERSION)
url=https://github.com/prometheus/node_exporter/releases/download/v${version}/node_exporter-${version}.linux-amd64.tar.gz
mkdir /opt/prometheus
wget -O- $url | tar xz -C /opt/prometheus --strip-components 1
chown prometheus:prometheus /opt/prometheus/node_exporter
ln -s /opt/prometheus/prometheus /usr/local/bin/prometheus

if [ ! -f /etc/systemd/system/node_exporter.service ];then
cat > /etc/systemd/system/node_exporter.service << EOF
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/opt/prometheus/node_exporter

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable node_exporter --now
fi