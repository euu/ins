#!/bin/sh
# Install latest Terraform
set -eu
url=$(wget -qO- https://www.terraform.io/downloads.html \
 | grep -oP 'https:\/\/releases\.hashicorp\.com\/terraform\/([0-9\.]+)\/terraform_([0-9\.]+)_linux_amd64\.zip')
version=$(echo $url | grep -Po '(?<=terraform\/)\d+.\d+.?\d+')
if type terraform 1>/dev/null;then
  installed=$(terraform --version | grep -Po '\d+.\d+.?\d+')
  if [ "$version" = "$installed" ]; then
  echo There\'s no need to update
  exit 0
  fi
fi
echo Installing Terraform $version
wget $url -O /tmp/terraform.zip
sudo unzip -o /tmp/terraform -d /opt
sudo ln -s /opt/terraform /usr/local/bin/terraform
rm /tmp/terraform.zip
terraform --version