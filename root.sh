# sublime
wget -qO- https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor > /etc/apt/trusted.gpg.d/sublime.gpg
echo "deb https://download.sublimetext.com/ apt/stable/" > /etc/apt/sources.list.d/sublimehq.list

# vscode
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /etc/apt/trusted.gpg.d/packages.microsoft.gpg
echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list

# google chrome
wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list

# skype
wget -qO- https://repo.skype.com/data/SKYPE-GPG-KEY | gpg --dearmor > /etc/apt/trusted.gpg.d/skype.gpg
echo "deb [arch=amd64] https://repo.skype.com/deb stable main" > /etc/apt/sources.list.d/skype-stable.list

# cloudflare
wget -qO- https://pkg.cloudflareclient.com/pubkey.gpg | gpg --dearmor > /etc/apt/trusted.gpg.d/cloudflare-client.gpg
echo 'deb [arch=amd64] http://pkg.cloudflareclient.com/ focal main' > /etc/apt/sources.list.d/cloudflare-client.list

apt-get install -y apt-transport-https
apt update
apt-get install -y code \
                   curl \
                   cloudflare-warp \
                   byobu \
                   dos2unix \
                   doublecmd-gtk \
                   evince \
                   fonts-firacode \
                   git \
                   gnome-tweaks \
                   gnome-shell-extension-dash-to-panel \
                   jq \
                   google-chrome-stable \
                   keepassx \
                   mupdf \
                   neovim \
                   nmap \
                   p7zip-full \
                   sox libsox-fmt-all \
                   sublime-text 

# Remove snapd 
systemctl stop snapd && systemctl disable snapd && apt purge snapd
rm -rf ~/snap
rm -rf /snap /var/snap /var/lib/snapd /var/cache/snapd /usr/lib/snapd

echo "# To install snapd, specify its version with 'apt install snapd=VERSION'
# where VERSION is the version of the snapd package you want to install.
Package: snapd
Pin: release a=*
Pin-Priority: -10''' > /etc/apt/preferences.d/no-snap.pref" > /etc/apt/preferences.d/no-snap.pref
