#!/bin/sh
if [ "$USER" != 'root' ];then echo "Run script as root" exit 1; fi

swapoff -a
sed -i '/ swap / s/^/#/' /etc/fstab

if [ "$(lsb_release -cs)" != "xenial" ];then
apt-get install -y iptables arptables ebtables # ensure legacy binaries are installed
update-alternatives --set iptables /usr/sbin/iptables-legacy # switch to legacy versions
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
update-alternatives --set arptables /usr/sbin/arptables-legacy
update-alternatives --set ebtables /usr/sbin/ebtables-legacy
fi
export DEBIAN_FRONTEND=noninteractive  # remove errors from Vagrant console
apt-get update && apt-get install -y apt-transport-https curl bash-completion
wget --https-only -qO- https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main">/etc/apt/sources.list.d/kubernetes.list
apt-get update && apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl