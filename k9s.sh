#!/bin/sh
# Install latest k9s
set -eu
url="$(wget -qO- https://github.com/derailed/k9s/releases \
  | grep -oP '\/derailed\/k9s\/releases\/download\/v([0-9\.]+)\/k9s_Linux_x86_64\.tar\.gz' | head -1 )"
last_version=$(echo $url | grep -Po '(?<=download\/v)\d+.\d+.\d+')
if type k9s >/dev/null;then
  installed_version=$(k9s version -s | sed -n 2p | awk '{print $2}') 
  if [ "$last_version" = "$installed_version" ];then
    echo Do not need to update, last version $last_version installed
    exit 0
  fi
fi
echo k9s $last_version available
sudo wget --show-progress --https-only -qO- "https://github.com/$url" \
| sudo tar xz -C /usr/local/bin/ --no-same-owner --wildcards --no-anchored 'k9s'
k9s version