#!/bin/sh
# Install latest Helm
url="$(wget -qO- https://github.com/helm/helm/releases | grep -oP 'https:\/\/get\.helm\.sh\/helm-v3([0-9\.]+)-linux-amd64\.tar\.gz' | head -1 )"
last_version=$(echo $url | grep -Po '(?<=helm-)v\d+.\d+.\d+')
if type helm 1>/dev/null;then
  installed_version=$(helm version --template="{{ .Version }}") 
  if [ "$last_version" = "$installed_version" ];then
    echo Do not need to update, last version $last_version installed
    exit 0
  fi
fi
echo Helm $last_version available
sudo wget --show-progress --https-only -O- $url | sudo tar xz -C /usr/local/bin --strip-components 1  --wildcards --no-anchored 'helm'
helm version