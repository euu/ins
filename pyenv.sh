wget -qO- https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
if ! grep -q PYENV_ROOT ~/.profile
then
{   echo 'export PYENV_ROOT="$HOME/.pyenv"'
    echo 'export PATH="$PYENV_ROOT/bin:$PATH"'
} >> ~/.profile
fi