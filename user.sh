code --install-extension ms-python.python
code --install-extension golang.go
code --install-extension pkief.material-icon-theme
code --install-extension formulahendry.code-runner
# code --install-extension peterjausovec.vscode-docker
# code --install-extension googlecloudtools.cloudcode
# code --install-extension ms-kubernetes-tools.vscode-kubernetes-tools

gsettings set org.gnome.desktop.interface cursor-size 32
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.wm.keybindings switch-input-source "['<Shift>Alt_L']"
gsettings set org.gnome.desktop.wm.keybindings switch-input-source-backward "['<Alt>Shift_L']"


git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
git config --global core.editor "code --wait"
git config --global pull.rebase true
git config --global alias.review '!bash -c "git push origin HEAD:refs/for/$1" -'
git config --global --add --bool push.autoSetupRemote true

curl -L https://github.com/gokcehan/lf/releases/latest/download/lf-linux-amd64.tar.gz | tar xzC ~/.local/bin

sudo apt install fzf
