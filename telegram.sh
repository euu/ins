#!/bin/bash
if [ -z "$1" ]
  then
    echo "Downloading without proxy"
    wget -O- https://telegram.org/dl/desktop/linux | tar xJ -C /opt
  else
    echo "Downloading with proxy"
    curl https://telegram.org/dl/desktop/linux --proxy $1 | tar xJ -C /opt
fi
ln -s /opt/Telegram/Telegram /usr/local/bin/telegram-desktop