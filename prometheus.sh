# Install latest Prometheus
version=$(wget -qO- https://raw.githubusercontent.com/prometheus/prometheus/master/VERSION)
echo Downloading Prometheus $version...
wget https://github.com/prometheus/prometheus/releases/download/v${version}/prometheus-${version}.linux-amd64.tar.gz
mkdir /opt/prometheus
wget -O- $url | tar xz -C /opt/prometheus --strip-components 1
ln -s /opt/prometheus/prometheus /usr/local/bin/prometheus
ln -s /opt/prometheus/promtool /usr/local/bin/promtool
adduser --no-create-home --disabled-login --shell /bin/false --gecos "Prometheus Monitoring User" prometheus

mkdir /etc/prometheus /var/lib/prometheus
rm /opt/prometheus/prometheus.yml
chown -R prometheus:prometheus /var/lib/prometheus
chown -R prometheus:prometheus /etc/prometheus

if [ ! -f /etc/systemd/system/prometheus.service ];then
cat > /etc/systemd/system/prometheus.service << EOF
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
EOF
fi

if [ ! -f /etc/prometheus/prometheus.yml ];then
cat > /etc/prometheus/prometheus.yml << EOF
global:
  scrape_interval: 15s

rule_files:
  - 'prometheus.rules.yml'

scrape_configs:

  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']

  - job_name: 'node_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9100']

  - job_name: 'blackbox_exporter'
    scrape_interval: 5s
    static_configs:
      - targets:
        - localhost:9115
        labels:
          group: 'production'

  - job_name: 'grafana'
    scrape_interval: 5s
    static_configs:
      - targets:
        - localhost:3000

alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - localhost:9093
EOF
fi

if [ ! -f /etc/prometheus/prometheus.rules.yml ];then
cat > /etc/prometheus/prometheus.rules.yml << EOF
groups:
  - name: example_alert
    rules:
      - alert: InstanceDown
        expr: up == 0
        for: 5m
        labels:
          severity: page
        annotations:
          summary: "Instance {{ $labels.instance }} down"
          description: "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 5 minutes."
EOF
fi
systemctl daemon-reload
systemctl enable prometheus --now
echo Prometheus $version installed