echo -n "Install Virtualbox repo (y/n)? ";read answer
if [ "$answer" != "${answer#[Yy]}" ];then
sudo wget --https-only -qO- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian \
   $(lsb_release -cs) contrib"
sudo apt update && sudo apt install virtualbox-6.1
echo Installed $(vboxmanage --version)
echo $(virtualbox --help | head -n 1 | awk '{print $NF}')
fi

echo -n "Install last Vagrant (y/n)? ";read answer
if [ "$answer" != "${answer#[Yy]}" ];then
echo 'Install latest Vagrant for Debian'
url="$(wget -qO- https://www.vagrantup.com/downloads.html|grep -oP 'https:\/\/releases\.hashicorp\.com\/vagrant\/([0-9\.]+)\/vagrant_([0-9\.]+)_x86_64\.deb' | head -1 )"
wget --show-progress --https-only -q "$url" -O /tmp/vagrant.deb && sudo apt install /tmp/vagrant.deb && rm /tmp/vagrant.deb
vagrant version
fi